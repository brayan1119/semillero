import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InicioSesionService } from 'src/app/services/inicio-sesion/inicio-sesion.service';
import { InicioSesion } from 'src/app/dto/inicio-sesion';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.scss']
})
export class InicioSesionComponent implements OnInit, OnDestroy {

  inicioSesionFrom: FormGroup;
  nombre = 'Brayan';

  constructor(private servicio: InicioSesionService,
    private router: Router) { }

  ngOnInit() {
    this.crearFormulario();
  }

  crearFormulario() {
    this.inicioSesionFrom = new FormGroup({
      nombreUsuario: new FormControl(null, [Validators.required, Validators.minLength(2)]),
      clave: new FormControl(null, Validators.required)
    });
  }

  enviarFormulario() {
    this.servicio.iniciarSesion(this.inicioSesionFrom.value).subscribe(datos =>{
      console.log(datos);

      sessionStorage.setItem('token', datos.token);
      this.router.navigate(['/plantilla/pagina-principal']);
    }
    , error => {
      console.error(error);
    }, () => {
      console.log('complete');
    });
  }

  get nombreUsuario() { return this.inicioSesionFrom.get('nombreUsuario'); }

  ngOnDestroy(): void {
    // throw new Error('Method not implemented.');
  }
}
