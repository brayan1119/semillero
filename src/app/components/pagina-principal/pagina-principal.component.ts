import { Component, OnInit } from '@angular/core';
import { PaginaPrincipalService } from 'src/app/services/pagina-principal/pagina-principal.service';
import { Usuario } from 'src/app/dto/usuario';

@Component({
  selector: 'app-pagina-principal',
  templateUrl: './pagina-principal.component.html',
  styleUrls: ['./pagina-principal.component.scss']
})
export class PaginaPrincipalComponent implements OnInit {

  usuarios: Usuario[];
  cedula: string;

  constructor(private servicio: PaginaPrincipalService) {  }

  ngOnInit() {
    this.obtenerUsuarios();
  }

  obtenerUsuarios() {
    this.servicio.obtenerUsuarios().subscribe(usuarios => this.usuarios = usuarios);
  }

  cambiocedula() {

  }
}
