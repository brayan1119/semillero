import { Pipe, PipeTransform } from '@angular/core';
import { Usuario } from 'src/app/dto/usuario';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(usuarios: Usuario[], texto: string): Usuario[] {
    if (!texto || !usuarios) {
      return usuarios;
    }
    return usuarios.filter(usuario => usuario.cedula.toString().includes(texto.toString()));
  }

}
